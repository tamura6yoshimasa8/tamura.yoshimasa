/**
 *
 */
package controller;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

/**
 * @author tamura.yoshimasa
 *
 */
public class SignUpServletTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("BeforeClass");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("AfterClass");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		System.out.println("Before");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		System.out.println("After");
	}

	@Test
	public void testDoPost() {
//		リクエスト、レスポンスを生成
		MockHttpServletRequest req = new MockHttpServletRequest();
		MockHttpServletResponse resp = new MockHttpServletResponse();
//		サーブレット生成
		SignUpServlet target = new SignUpServlet();

//		リクエストにデータ設定
		req.setParameter( "loginId", "1111111" );
		req.setParameter( "password", "1111111" );
		req.setParameter( "password2", "1111111" );
		req.setParameter( "name", "1111111" );
		req.setParameter( "branchId", "1" );//東京支店
		req.setParameter( "departmentId", "1" );//総務人事

//		実行！
		try {
			target.init();
			target.doPost(req, resp);
			target.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			fail("Oops!!");
		}


//		結果をチェック
//		パラメータの確認、取得
		assertEquals( "loginId", "1111111", req.getParameter("loginId") );
		assertEquals( "password", "1111111", req.getParameter("password") );
		assertEquals( "password2", "1111111", req.getParameter("password2") );
		assertEquals( "name", "1111111", req.getParameter("name") );
		assertEquals( "branchId", "1", req.getParameter("branchId") );
		assertEquals( "departmentId", "1", req.getParameter("departmentId") );

////		セッションの中身確認
//		HttpSession session = req.getSession();
//		assertEquals("ans","3",session.getAttribute("ans"));

////		フォワード先確認（response.getForwardedUrl()）
//		assertEquals("ans","/ans.jsp",resp.getForwardedUrl());

	}

}
