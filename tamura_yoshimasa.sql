CREATE DATABASE tamura_yoshimasa;

USE tamura_yoshimasa;

CREATE TABLE users(
id int(20) PRIMARY KEY NOT NULL UNIQUE AUTO_INCREMENT,
login_id VARCHAR(20) NOT NULL,
password VARCHAR(255) NOT NULL,
name VARCHAR(10) NOT NULL,
branch_id int(20) NOT NULL,
department_id int(20) NOT NULL,
is_user int(1) NOT NULL,
created_date TIMESTAMP NOT NULL,
updated_date TIMESTAMP NOT NULL
);


CREATE TABLE branchs(
id int(20) PRIMARY KEY NOT NULL UNIQUE AUTO_INCREMENT,
branch_name VARCHAR(20) NOT NULL
);

CREATE TABLE departments(
id int(20) PRIMARY KEY NOT NULL UNIQUE AUTO_INCREMENT,
department_name VARCHAR(20) NOT NULL
);

CREATE TABLE messages(
id int(20) PRIMARY KEY NOT NULL UNIQUE AUTO_INCREMENT,
user_id int(20) NOT NULL,
title VARCHAR(30) NOT NULL,
text VARCHAR(1000) NOT NULL,
category VARCHAR(10) NOT NULL,
created_date TIMESTAMP NOT NULL,
updated_date TIMESTAMP NOT NULL
);

CREATE TABLE comments(
id int(20) PRIMARY KEY NOT NULL UNIQUE AUTO_INCREMENT,
user_id int(20) NOT NULL,
message_id int(20) NOT NULL,
text VARCHAR(500) NOT NULL,
ceated_date TIMESTAMP NOT NULL,
updated_date TIMESTAMP NOT NULL
);
