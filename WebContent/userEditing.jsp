<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<link href="./css/formScreen.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー編集</title>
</head>
	<body>
		<div class="header">
			<div class="title">ユーザー編集</div>
			<div class="header-href">
			<a href="userManagement">ユーザー管理</a>
			</div>

		</div>
		<div class="main-contents">
			<div class="VariousMessages">
				<c:if test="${ not empty errorMessages}">
					<div class="errorMessages">
						<ul>
							<c:forEach items="${errorMessages }" var="message">
								<li><c:out value="${message}"/>
							</c:forEach>
						</ul>
					</div>
					<c:remove var="errorMessages" scope="session"/>
				</c:if>
			</div>




			<div class="form-contents">
			<form action="editing" method="post" >

                <input name="userId" value="${editUser.id}" id="userId" type="hidden"/>

                <label for="loginId">ログインID（半角英数字6文字以上20文字以下）</label><br/>
                <input name="loginId" id=loginId value="${editUser.loginId}" /> <br />

                <label for="password">パスワード（半角文字6文字以上20文字以下）</label><br/>
                <input name="password" type="password" id="password" value="" /> <br />

                <label for="password2">パスワード(確認用)</label><br/>
                <input name="password2" type="password" id="password2" value="" /> <br />

				<label for="name">名前（10文字以下）</label><br/>
				<input name="name" id="name" value="${editUser.name}"  /><br />

				<c:if test="${editUser.id == loginUser.id }">
				<input type="hidden" name="branchId" value="${editUser.branchId}">
				<input type="hidden" name="departmentId" value="${editUser.departmentId}">
				</c:if>



                <c:if test="${editUser.id != loginUser.id }">
               		支店
				<select name="branchId">
					<c:forEach items="${branch}" var="branch">
						<c:if test="${branch.branchId eq editUser.branchId}">
							<option  id="branchId" value="${branch.branchId}" selected>${branch.branchName}</option>
						</c:if>
						<c:if test="${branch.branchId ne editUser.branchId}">
							<option  id="branchId" value="${branch.branchId}">${branch.branchName}</option>
						</c:if>
	              	</c:forEach>
				</select><br/>

                部署・役職
				<select name="departmentId">
					<c:forEach items="${department}" var="department">
						<c:if test="${department.departmentId eq editUser.departmentId}">
							<option id="departmentId" value="${department.departmentId}" selected>${department.departmentName}</option>
						</c:if>
						<c:if test="${department.departmentId ne editUser.departmentId}">
							<option id="departmentId" value="${department.departmentId}">${department.departmentName}</option>
						</c:if>
	              	</c:forEach>
				</select><br/>
               	</c:if>




                <input type="submit" value="登録" /> <br />
            </form>
            </div>


			<div class="copyright">Copyright(c)Tamura Yoshimasa</div>


		</div>

	</body>
</html>
