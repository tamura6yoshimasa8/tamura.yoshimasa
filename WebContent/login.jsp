<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ログイン</title>
</head>
<body>
	<div class="main-contents">
		<div class="login-contents">
            <div class="title"><h1>ALH 掲示板</h1></div>

			<div class="VariousMessages">
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>
            </div>

            <form action="login" method="post" ><br />
                <label for="loginId">ログインID</label><br/>
                <input name="loginId" id="loginId" value="${loginUser.loginId}"/> <br />

                <label for="password">パスワード</label><br/>
                <input name="password" type="password" id="password" value=""/> <br />
				<div id="add-login">
                <input type="submit" value="ログイン"  /> <br />
                <span></span>
                </div>
            </form>
		</div>

		<div class="copyright"> Copyright(c)TamuraYoshimasa</div>

	</div>

</body>
</html>