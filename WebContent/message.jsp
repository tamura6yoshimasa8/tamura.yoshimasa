<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<link href="./css/formScreen.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>投稿画面</title>
</head>
<body>
	<div class="header">
		<div class="title">新規投稿</div>
		<div class="header-href">
		<a href="./">ホーム</a>
		</div>
	</div>
	<div class="main-contents">
		<div class="VariousMessages">
			<c:if test="${ not empty errorMessages }">
	                <div class="errorMessages">
	                    <ul>
	                        <c:forEach items="${errorMessages}" var="message">
	                            <li><c:out value="${message}" />
	                        </c:forEach>
	                    </ul>
	                </div>
	                <c:remove var="errorMessages" scope="session"/>
			</c:if>
		</div>

		<div class="form-contents">
		<form action="newMessage" method="post" >
			<label for="title">件名:（30文字以下）</label><br/>
			<input type="text"name="title" id="title" value="${newMessage.title}"/><br/>

			<label for="text">本文:（1000文字以下）</label><br/>
			<textarea name="text" cols="55" rows="10" id="text" ><c:out value="${newMessage.text}"/></textarea><br/>

			<label for="category">カテゴリー:（10文字以下）</label><br/>
			<input type="text"name="category" id="category" value="${newMessage.category}"/><br/>

			<input type="submit" value="投稿" /> <br />


		</form>
		</div>

		<div class="copyright"> Copyright(c)TamuraYoshimasa</div>
	</div>

</body>
</html>