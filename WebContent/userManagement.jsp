<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/managementStyle.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理画面</title>

<!-- 確認機能 -->
<script type="text/javascript">
function check(param, userName){
	if(window.confirm(userName + 'さんのアカウントを' + param + 'させてよろしいですか？')){ // 確認ダイアログを表示
		return true; // 「OK」時は送信を実行
	}
	else{ // 「キャンセル」時の処理
		return false; // 送信を中止
	}
}

</script>


</head>
    <body>


       	<div class="header">
           	<div class="title">ユーザー管理</div>
           	<div class="header-href">
           	<a href="./">ホーム</a>
            <a href="signup">ユーザー新規登録</a>
	        </div>
		</div>
		<div class="main-contents">
			<div class="VariousMessages">
        	 <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>

            <c:if test="${ not empty deleteMessage}">
            	<div class="deleteMessage">
                    <ul>
                     	<c:forEach items="${deleteMessage}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="deleteMessage" scope="session"/>
            </c:if>

            <c:if test="${ not empty Messages }">
                <div class="Messages">
                    <ul>
                        <c:forEach items="${Messages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="Messages" scope="session"/>
            </c:if>
            </div>




			<!-- ユーザー一覧を表示 -->
			<div class="users">
				<table border="1">
					<tr>
						<th>ID</th>
						<th>ログインID</th>
						<th>名前</th>
						<th>支店</th>
						<th>部署・役職</th>
						<th>停止・復活</th>
						<th>登録日時</th>
						<th>情報変更日時</th>
						<th>編集</th>

						<!--  <th>削除</th> -->

					</tr>

				    <c:forEach items="${users}" var="users">
			            	<br/>
			                <!-- ユーザー表示 -->
			                <tr>
			                <td><c:out value="${users.id}" /></td>
			                <td><c:out value="${users.loginId}" /></td>
			                <td><c:out value="${users.name}" /></td>
			                <td><c:out value="${users.branchName}" /></td>
			                <td><c:out value="${users.departmentName}" /></td>
			                <td><form action="userCondition" method="post">
			                	<c:if test="${users.id == loginUser.id }">
			                	ログイン中
			                	</c:if>

			                	<c:if test="${users.id != loginUser.id }">
				                	<c:if test="${users.isUser == 0 }">
					                	<input type="hidden" name="isUser" id="isUser" value="1">
					                	<input type="submit" class="isUser" value="停止" onClick="return check('停止','${users.name}')">
					                </c:if>
					                <c:if test="${users.isUser == 1 }">
					                	<input type="hidden" name="isUser" id="isUser" value="0">
					                	<p>-停止中-</p>
					                	<input type="submit" class="isUser" value="復活" onClick="return check('復活','${users.name}')" >
					                </c:if>
				                	<input type="hidden" name="userId" id="userId" value="${users.id}">
				                	<input type="hidden" name="userName" id="userName" value="${users.name}">
			                	</c:if>

			                </form></td>
			                <td><div class="createdDate"><fmt:formatDate value="${users.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div></td>
			                <td><div class="updatedDate"><fmt:formatDate value="${users.updatedDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div></td>
			                <td>
			                	<a href="editing?userId=${users.id}">編集</a>
			                </td>

			                <!--  <td>
			                <form action="userDelete" method="post">
			                	<c:if test="${users.id == loginUser.id }">
			                		ログイン中
			                	</c:if>
			                	<c:if test="${users.id != loginUser.id }">
				                	<input type="submit" class="delete" value="削除" onClick="return check('${users.name}')" >
				                	<input type="hidden" name="userId" id="userId" value="${users.id}">
				                	<input type="hidden" name="userName" id="userName" value="${users.name}">
			                	</c:if>
			                </form>
			                </td> -->

							</tr>

				    </c:forEach>
			    </table>
			</div>

			<br/>


            <div class="copyright"> Copyright(c)TamuraYoshimasa</div>
        </div>
    </body>


</html>