<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<script src="//code.jquery.com/jquery-3.2.1.min.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>掲示板</title>


<!-- 削除機能 -->
<script type="text/javascript">
function check(param){
	if(window.confirm(param + 'を削除してよろしいですか？')){ // 確認ダイアログを表示
		return true; // 「OK」時は送信を実行
	}
	else{ // 「キャンセル」時の処理
		return false; // 送信を中止
	}
}
</script>

<script>

history.forward();


</script>

</head>
    <body>

       	<div class="header">
          	<div class="title">ホーム</div>
          	<div class="header-href">
          	 <a href="./">ホーム</a>
		        <a href="newMessage">新規投稿</a>
		        <c:if test="${loginUser.departmentId eq 1 }">
		        	<a href="userManagement">ユーザー管理</a>
		        </c:if>

		        <div class="logout">
		        <a href="logout">ログアウト</a>

		        </div>
        	</div>

 			<script>
        		$('.aticle').on({
        		  mouseenter: function() {
        		    $(this).animate({ opacity: 0.5 }, 600, 'swing');
        		  },
        		  mouseleave: function() {
        		    $(this).animate({ opacity: 1 }, 300, 'swing');
        		  }
        		});
        	</script>


		    <div class="aticle">
			<!-- 日付絞込み機能 -->


			<!-- 折り畳み展開ポインタ -->
				<div onclick="obj=document.getElementById('open').style; obj.display=(obj.display=='none')?'block':'none';">
				<a style="cursor:pointer;">▼ 検索</a>
				</div>
				<!--// 折り畳み展開ポインタ -->

				<!-- 折り畳まれ部分 -->
				<div id="open" style="display:none;clear:both;">

				<form action="./" >
				<div class="aticle-category">
				カテゴリー検索
				<input type="text" id="category" name="category" value="${retrieval.category}"></input>
				</div>
				<div class="aticle-date">
				日付検索
				<input type="date" id="beforeDate" name="beforeDate" value="${retrieval.beforeDate}"></input>～
				<input type="date" id="afterDate" name="afterDate" value="${retrieval.afterDate}"></input><br/>
				</div>
				<input type="submit" value="検索">
				</form><br/>
				<span></span>
				</div>
				<!--// 折り畳まれ部分 -->
			</div>
			<script>
				$('.aticle a').on({
				  mouseenter: function() {
				    $(this).animate({ opacity: 0.5 }, 300, 'swing');
				  },
				  mouseleave: function() {
				    $(this).animate({ opacity: 1 }, 300, 'swing');
				  }
				});
			</script>
		</div>
		<div class="main-contents">

        	<div class="VariousMessages">
        	<c:if test="${ not empty retrievalMessages }">
		        <div class="retrievalMessages">
		            <ul>
		                <c:forEach items="${retrievalMessages}" var="message">
		                    <li><c:out value="${message}" />
		                </c:forEach>
		            </ul>
		        </div>
		        <c:remove var="retrievalMessages" scope="session"/>
			</c:if>

	        <c:if test="${ not empty commentMessages }">
		        <div class="errorMessages">
		            <ul>
		                <c:forEach items="${commentMessages}" var="message">
		                    <li><c:out value="${message}" />
		                </c:forEach>
		            </ul>
		        </div>
		        <c:remove var="commentMessages" scope="session"/>
			</c:if>

			<c:if test="${ not empty Messages }">
		        <div class="Messages">
		            <ul>
		                <c:forEach items="${Messages}" var="message">
		                    <li><c:out value="${message}" />
		                </c:forEach>
		            </ul>
		        </div>
		        <c:remove var="Messages" scope="session"/>
			</c:if>

	       	<c:if test="${ not empty errorMessages }">
		        <div class="errorMessages">
		            <ul>
		                <c:forEach items="${errorMessages}" var="message">
		                    <li><c:out value="${message}" />
		                </c:forEach>
		            </ul>
		        </div>
		        <c:remove var="errorMessages" scope="session"/>
			</c:if>

			<c:if test="${ not empty deleteMessage}">
			<div class="deleteMessages">
	       		<ul>
				<c:forEach items="${deleteMessage}" var="message">
			        <li><c:out value="${message}" />
			    </c:forEach>
			    </ul>
			</div>
			<c:remove var="deleteMessage" scope="session"/>
			</c:if>
			<br/>



			<!-- つぶやいたメッセージを表示 -->
			<div class="messages">
			    <c:forEach items="${messages}" var="message">
			    	<hr>
		            <div class="message">

		            	<br/>
		                <div class="account-name">
		                    <span class="name"><c:out value="${message.userName}" />さんの投稿</span>
		                </div>
		                <!-- 投稿表示 -->

						<div class="messageContent">
		                <div class="messageTitle">件名:<a><c:out value="${message.messageTitle}" /></a></div>
		                <div class="messageText">本文<pre><c:out value="${message.messageText}" /></pre></div>
		                <div class="messageCategory">カテゴリー:<c:out value="${message.messageCategory}" /></div>
		                <div class="messageDate"><fmt:formatDate value="${message.messageCreatedDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div><br/>
		                <!-- 投稿削除機能 -->
		                <c:if test="${ loginUser.id eq message.userId }">
			                <form action="messageDelete" method="post" >
								<div class="messageId">
								<input type="hidden" name="messageId" id= "messageId" value="${message.messageId}" />
								<input type="hidden" name="messageName" id= "messageName" value="${message.userName}" />
								<input type="submit" value="投稿削除" onClick="return check('投稿')" id= "messageSubmit"/> <br />
								</div>
							</form>
						</c:if>
						</div>


	               		<!-- コメント表示 -->
	               		<div class="comment">
		                <c:forEach items="${comments}" var="comment">

			    			<c:if test="${message.messageId eq comment.messageId }">
			    			<div class="comments">
				                <div class="userName"><c:out value="${comment.userName}" />さんのコメント</div>
		                 		<div class="commentText"><pre><c:out value="${comment.commentText}" /></pre></div>
				                <div class="commentDate"><fmt:formatDate value="${comment.commentCreatedDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>

				                <!-- コメント削除機能 -->
   				    			<c:if test="${ loginUser.id eq comment.userId }">
					                <form action="commentDelete" method="post"  >
										<div class="commentId">
										<input type="hidden" name="commentId" id= "commentId" value="${comment.commentId}" />
										<input type="hidden" name="messageUserName" id= "messageUserName" value="${message.userName}" />
										<input type="submit" value="コメント削除" onClick="return check('コメント')"/> <br /><br/>
										</div>
									</form>
								</c:if>
							</div>
							</c:if>

						</c:forEach>
						<!-- コメント機能 -->
						<form action="commentText" method="post" >
							<div class="commentRecord">
	              				<pre><textarea name="commentText" cols="35" rows="5" id="commentText" ><c:if test="${ not empty commentText && message.messageId eq commentText.messageId}"><c:out value="${commentText.commentText}"/></c:if></textarea></pre>
							<input type="hidden" name="messageId" id= "messageId" value="${message.messageId }" />
							<input type="submit" value="コメント" id= "commentSubmit" />
							</div>
						</form>
						</div>
						<script>
						$(function() {
						    // 登録ボタンを押したら
						    $('#commentSubmit').on('click', function() {
						        // 送信するデータを用意
						        var codeDate = $('#commentText').val();
						        var nameDate = $('#messageId').val();
						        var user = { 'commentText': commentTextDate, 'messageId': messageIdDate };

						        // Ajax通信処理
						        $.ajax({
						            // レスポンスをJSONとしてパースする
						            dataType: 'json',
						            // POSTで通信する
						            type: 'POST',
						            // POST送信先のURL
						            url: 'user/commentText',
						            // JSONデータ本体
						            data: { user: JSON.stringify(user) }
						        }).done(function(data) {
						            // 成功時の処理
						            console.log('success!!');
						        }).fail(function(data) {
						            // 失敗時の処理
						            console.log('error!!');
						        });
						    });
						});
						</script>
		            </div>

			    </c:forEach>
			    <c:remove var="commentText" scope="session"/>

			</div>

			<div class="copyright"> Copyright(c)TamuraYoshimasa</div>
			</div>
		</div>
    </body>
</html>