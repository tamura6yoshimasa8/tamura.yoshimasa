<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<link href="./css/style.css" rel="stylesheet" type="text/css">
	<link href="./css/formScreen.css" rel="stylesheet" type="text/css">
	<script src="//code.jquery.com/jquery-3.2.1.min.js"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザー登録</title>
	</head>
	<body>
		<div class="header">
			<div class="title">ユーザー新規登録</div>
			<div class="header-href">
			<a href="userManagement">ユーザー管理</a>
			</div>
		</div>
		<div class="main-contents">
			<div class="VariousMessages">

				<c:if test="${ not empty errorMessages}">
					<div class="errorMessages">
						<ul>
							<c:forEach items="${errorMessages }" var="message">
								<li><c:out value="${message}"/>
							</c:forEach>
						</ul>
					</div>
					<c:remove var="errorMessages" scope="session"/>
				</c:if>
			</div>


			<div class="form-contents">
			<form action="signup" method="post" id="form">


                <label for="loginId">ログインID（半角英数字6文字以上20文字以下）</label><br/>
                <input name="loginId" id=loginId value="${signUpUser.loginId}" /> <br />

                <label for="password">パスワード（半角文字6文字以上20文字以下）</label><br/>
                <input name="password" type="password" id="password"  /> <br />

                <label for="password2">パスワード（確認用）</label><br/>
                <input name="password2" type="password" id="password2"  /> <br />

				<label for="name">名前（10文字以下）</label><br/>
				<input name="name" id="name" value="${signUpUser.name}"  /><br />

                <label for="branchId">支店</label>
				<select name="branchId" id="branchId">
					<c:forEach items="${branch}" var="branch">
						<c:if test="${branch.branchId eq signUpUser.branchId}">
							<option  id="branchId" value="${branch.branchId}" selected>${branch.branchName}</option>
						</c:if>
						<c:if test="${branch.branchId ne signUpUser.branchId}">
							<option  id="branchId" value="${branch.branchId}">${branch.branchName}</option>
						</c:if>
	              	</c:forEach>
				</select><br/>

                <label for="departmentId">部署・役職</label>
				<select name="departmentId" id="departmentId">
					<c:forEach items="${department}" var="department">
						<c:if test="${department.departmentId eq signUpUser.departmentId}">
							<option id="departmentId" value="${department.departmentId}" selected>${department.departmentName}</option>
						</c:if>
						<c:if test="${department.departmentId ne signUpUser.departmentId}">
							<option id="departmentId" value="${department.departmentId}">${department.departmentName}</option>
						</c:if>
	              	</c:forEach>
				</select><br/>


                <input type="submit" value="登録" id="register" /> <br />

            </form>
            <script type="text/javascript">
            $(function() {
                // 登録ボタンを押したら
                $('button#register').on('click', function() {
                    // 送信するデータを用意
                    var codeDate = $('#loginId').val();
                    var nameDate = $('#name').val();
                    var user = { 'loginId': codeDate, 'name': nameDate };

                    // Ajax通信処理
                    $.ajax({
                        // レスポンスをJSONとしてパースする
                        dataType: 'json',
                        // POSTで通信する
                        type: 'POST',
                        // POST送信先のURL
                        url: '/sinup',
                        // JSONデータ本体
                        data: { user: JSON.stringify(user) }
                    }).done(function(data) {
                        // 成功時の処理
                        console.log('success!!');
                    }).fail(function(data) {
                        // 失敗時の処理
                        console.log('error!!');
                    });
                });
            });
            </script>

			</div>

			<div class="copyright">Copyright(c)Tamura Yoshimasa</div>


		</div>

	</body>
</html>
