package beans;

import java.io.Serializable;
import java.util.Date;

public class UserComment implements Serializable {
    private static final long serialVersionUID = 1L;

    private int userId;
    private String userName;
    private String userLoginId;
    private int commentId;
    private String commentText;
    private int messageId;
    private Date commentCreatedDate;
    private Date commentCpdatedDate;
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserLoginId() {
		return userLoginId;
	}
	public void setUserLoginId(String userLoginId) {
		this.userLoginId = userLoginId;
	}
	public int getCommentId() {
		return commentId;
	}
	public void setCommentId(int commentId) {
		this.commentId = commentId;
	}
	public String getCommentText() {
		return commentText;
	}
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}
	public int getMessageId() {
		return messageId;
	}
	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}
	public Date getCommentCreatedDate() {
		return commentCreatedDate;
	}
	public void setCommentCreatedDate(Date commentCreatedDate) {
		this.commentCreatedDate = commentCreatedDate;
	}
	public Date getCommentCpdatedDate() {
		return commentCpdatedDate;
	}
	public void setCommentCpdatedDate(Date commentCpdatedDate) {
		this.commentCpdatedDate = commentCpdatedDate;
	}
}
