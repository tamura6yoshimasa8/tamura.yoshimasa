package beans;

import java.io.Serializable;
import java.util.Date;

public class UserMessage implements Serializable {
    private static final long serialVersionUID = 1L;
//中身変更していない
    private int userId;
    private String userName;
    private String userLoginId;
    private int messageId;
    private String messageTitle;
    private String messageText;
    private String messageCategory;
    private Date messageCreatedDate;
    private Date messageCpdatedDate;
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserLoginId() {
		return userLoginId;
	}
	public void setUserLoginId(String userLoginId) {
		this.userLoginId = userLoginId;
	}
	public int getMessageId() {
		return messageId;
	}
	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}
	public String getMessageText() {
		return messageText;
	}
	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}
	public String getMessageCategory() {
		return messageCategory;
	}
	public void setMessageCategory(String messageCategory) {
		this.messageCategory = messageCategory;
	}
	public Date getMessageCreatedDate() {
		return messageCreatedDate;
	}
	public void setMessageCreatedDate(Date messageCreatedDate) {
		this.messageCreatedDate = messageCreatedDate;
	}
	public Date getMessageCpdatedDate() {
		return messageCpdatedDate;
	}
	public void setMessageCpdatedDate(Date messageCpdatedDate) {
		this.messageCpdatedDate = messageCpdatedDate;
	}
	public String getMessageTitle() {
		return messageTitle;
	}
	public void setMessageTitle(String messageTitle) {
		this.messageTitle = messageTitle;
	}

    // getter setter は省略

}