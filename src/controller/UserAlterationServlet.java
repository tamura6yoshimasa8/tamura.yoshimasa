//package controller;
//
//
//import java.io.IOException;//4
//import java.util.ArrayList;
//import java.util.List;
//
//import javax.servlet.ServletException;//4
//import javax.servlet.annotation.WebServlet;//2
//import javax.servlet.http.HttpServlet;//1
//import javax.servlet.http.HttpServletRequest;//3
//import javax.servlet.http.HttpServletResponse;//3
//import javax.servlet.http.HttpSession;
//
//import org.apache.commons.lang.StringUtils;
//
//import beans.User;
//import service.UserService;
//
//
//@WebServlet(urlPatterns = {"/alteration"})
//public class UserAlterationServlet extends HttpServlet {
//	private static final long serialVersionUID = 1L;
//
//	@Override
//	protected void doGet(HttpServletRequest request ,
//			HttpServletResponse response) throws IOException ,ServletException{
//
//		request.getRequestDispatcher("userManagement.jsp").forward(request, response);
//
//	}
//	@Override
//	protected void doPost(HttpServletRequest request,
//			HttpServletResponse response) throws IOException, ServletException {
//
//		List<String> messages = new ArrayList<String>();
//		HttpSession session = request.getSession();
//
//		if(isValid(request,messages) == true){
//
//			User user = new User();
//			user.setId(Integer.parseInt(request.getParameter("id")));
//			user.setLoginId(request.getParameter("loginId"));
//			if(request.getParameter("password") != null){
//				user.setPassword(request.getParameter("password"));
//			}
//			user.setName(request.getParameter("name"));
//			user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
//			user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId"))) ;
//
//			new UserService().update(user);//変更
//
//			response.sendRedirect("userManagement");
//		}else{
////			User signUpUser = getSignUpUser(request);
//			session.setAttribute("errorMessages", messages);
//			response.sendRedirect("editing");
////			request.setAttribute("signUpUser", signUpUser);
////			request.getRequestDispatcher("signup.jsp").forward(request,response);
//		}
//	}
//
//
//	//記入がなかったらエラーをはくメソッド
//	private boolean isValid(HttpServletRequest request, List<String> messages) {
//        String loginId = request.getParameter("loginId");
//        String name = request.getParameter("name");
//        String branchId = request.getParameter("branchId");
//        String departmentId = request.getParameter("departmentId");
//
//        if (StringUtils.isEmpty(loginId) == true) {
//            messages.add("ログインIDを入力してください");
//        }
//        if (StringUtils.isEmpty(name) == true) {
//            messages.add("名前を入力してください");
//        }
//        if (StringUtils.isEmpty(branchId) == true) {
//            messages.add("支店名IDを入力してください");
//        }
//        if (StringUtils.isEmpty(departmentId) == true) {
//            messages.add("部署・役職IDを入力してください");
//        }
//        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
//        if (messages.size() == 0) {
//            return true;
//        } else {
//            return false;
//        }
//    }
//
//}
//
