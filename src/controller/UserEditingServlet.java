package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/editing" })
public class UserEditingServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    List<Department> department = new DepartmentService().getDepartment();//後に修正
    List<Branch> branch = new BranchService().getBranch();//後に修正

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

    	List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();

    	if(!StringUtils.isEmpty(request.getParameter("userId")) && request.getParameter("userId").matches("[0-9]{0,9}")){

    		int userId = Integer.parseInt(request.getParameter("userId"));
            User editUser = new UserService().getUser(userId);
            if(editUser != null ){
                request.setAttribute("editUser", editUser);
                request.setAttribute("department", department);
                request.setAttribute("branch", branch);
//                response.sendRedirect("userEditing.jsp");
                request.getRequestDispatcher("userEditing.jsp").forward(request, response);
    		}else{
    			messages.add("不正なアクセスです");
                session.setAttribute("errorMessages", messages);
        		response.sendRedirect("userManagement");
    		}
    	}else{
    		messages.add("不正なアクセスです");
            session.setAttribute("errorMessages", messages);
    		response.sendRedirect("userManagement");
    	}
    }


    //追加記述()
    @Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User editUser = new User();
		editUser = getEditUser(request);


        request.setAttribute("department", department);//後に修正
        request.setAttribute("branch", branch);//後に修正


		if(isValid(request,messages) == true){

			 try {
				 	new UserService().update(editUser);
	            } catch (NoRowsUpdatedRuntimeException e) {
	                messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
	                session.setAttribute("errorMessages", messages);
	                request.setAttribute("editUser", editUser);
	                request.getRequestDispatcher("userEditing.jsp").forward(request, response);
	                return;
	            }

	            //session.setAttribute("loginUser", editUser);

	            response.sendRedirect("userManagement");

		}else{

//			User editingUser = getEditingUser(request);
//			request.setAttribute("editingUser", editingUser);

			session.setAttribute("errorMessages", messages);
			session.setAttribute("editUser", editUser);
			request.getRequestDispatcher("userEditing.jsp").forward(request,response);
		}
	}

    private User getEditUser(HttpServletRequest request)
            throws IOException, ServletException {

        User editUser = new User();
        editUser.setId(Integer.parseInt(request.getParameter("userId")));
		editUser.setLoginId(request.getParameter("loginId"));
		if(!StringUtils.isBlank(request.getParameter("password"))){
			editUser.setPassword(request.getParameter("password"));
		}
		editUser.setName(request.getParameter("name"));
		editUser.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		editUser.setDepartmentId(Integer.parseInt(request.getParameter("departmentId"))) ;
        return editUser;
    }


	//記入がなかったらエラーをはくメソッド
    private boolean isValid(HttpServletRequest request, List<String> messages) {
    	int userId = Integer.parseInt(request.getParameter("userId"));
        String loginId = request.getParameter("loginId");
        String password = request.getParameter("password");
        String password2 = request.getParameter("password2");
        String name = request.getParameter("name");
        int branchId = Integer.parseInt(request.getParameter("branchId"));
        int departmentId = Integer.parseInt(request.getParameter("departmentId"));



        UserService userService = new UserService();
        User user = new User();
		user = userService.getUserLoginId(loginId);

        if (StringUtils.isEmpty(loginId) == true) {
        	messages.add("ログインIDを入力してください");
        }else if(loginId.length() < 6 || loginId.length() > 20 || !loginId.matches("[a-zA-Z0-9]+")){
    		messages.add("ログインIDは半角英数字の6文字以上20文字以下で入力してください");
		}

        if(user != null){
    		int id = user.getId();
    		if(id != userId){
    			messages.add("すでにこのログインIDは使用されています");
    		}
    	}

        if(StringUtils.isEmpty(password) == false){
        	if(password.length() < 6 || password.length() > 20 || !password.matches("[ -~。-゜A-Za-z0-9]+")){
            	messages.add("パスワードは半角文字の6文字以上20文字以下で入力してください");
            }
        }

        if(!password.equals(password2)){
        	messages.add("確認用のパスワードと合っていません");
        }

        if (StringUtils.isEmpty(name) == true) {
            messages.add("名前を入力してください");
        }else{
        	if(name.length() > 10){
            	messages.add("名前は10文字以内で入力してください");
            }
        }



        if((branchId >1 && departmentId <3) || (branchId ==1 && departmentId ==3) ){
        	messages.add("支店と部署・役職の組み合わせを正しく選択してください");
        }


        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

	 private User getEditingUser(HttpServletRequest request)
	            throws IOException, ServletException {


	    	String loginId = request.getParameter("loginId");
	    	String name = request.getParameter("name");

	        User loginUser = new User();

	        if(StringUtils.isEmpty(loginId) != true){
	        	loginUser.setLoginId(loginId);
	        }

	        if(StringUtils.isEmpty(name) != true){
	        	loginUser.setName(name);
	        }


	        return loginUser;
	    }


}
