package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import service.MessageService;

@WebServlet(urlPatterns = {"/messageDelete"})
public class MessageDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("top.jsp").forward(request, response);
    }

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException{

		int messageId= Integer.parseInt(request.getParameter("messageId")) ;

		MessageService messageService = new MessageService();
		messageService.messageDelete(messageId);//投稿削除

			HttpSession session = request.getSession();
//
			String message = "投稿を削除しました";
            session.setAttribute("deleteMessage", message);
//            request.getRequestDispatcher("./").forward(request, response);
            response.sendRedirect("./");



	}
}
