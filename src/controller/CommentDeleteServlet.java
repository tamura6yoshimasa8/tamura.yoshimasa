package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import service.CommentService;

@WebServlet(urlPatterns = {"/commentDelete"})
public class CommentDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("top.jsp").forward(request, response);
    }

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException{

		int commentId = Integer.parseInt(request.getParameter("commentId"));
		String messageUserName = request.getParameter("messageUserName");

		CommentService commentService = new CommentService();
		commentService.commentDelete(commentId);//投稿削除

			HttpSession session = request.getSession();
//
			String message = messageUserName+"さんへのコメントを削除しました";
            session.setAttribute("deleteMessage", message);
//            request.getRequestDispatcher("top.jsp").forward(request, response);
            response.sendRedirect("./");



	}
}
