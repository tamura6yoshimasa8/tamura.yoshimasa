package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.LoginService;

@WebServlet(urlPatterns = {"/login"})///login
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("/login.jsp").forward(request, response);
    }

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException{

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");


		LoginService loginService = new LoginService();//未実装
		User user = loginService.login(loginId ,password);

		HttpSession session = request.getSession();
        if (user != null) {
        	int isUser = user.getIsUser();
        	if(isUser == 0){
        		session.setAttribute("loginUser", user);
                response.sendRedirect("./");
        	}else{
        		 List<String> messages = new ArrayList<String>();
                 messages.add("ログインに失敗しました。");

                 User loginUser = getLoginUser(request);
                 session.setAttribute("errorMessages", messages);
                 request.setAttribute("loginUser", loginUser);//新規追加
                 request.getRequestDispatcher("login.jsp").forward(request, response);//新規作成

        	}
        }else {

            	User loginUser = getLoginUser(request);//新規作成
                List<String> messages = new ArrayList<String>();
                messages.add("ログインに失敗しました。");

                session.setAttribute("errorMessages", messages);
                request.setAttribute("loginUser", loginUser);//新規追加
                request.getRequestDispatcher("login.jsp").forward(request, response);//新規作成

        }

	}
	//新規追加
    private User getLoginUser(HttpServletRequest request)
            throws IOException, ServletException {


    	String loginId = request.getParameter("loginId");

        User loginUser = new User();

        if(StringUtils.isEmpty(loginId) != true){
        	loginUser.setLoginId(loginId);
        }


        return loginUser;
    }

}
