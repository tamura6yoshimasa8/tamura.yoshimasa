
package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//追加記述
import beans.User;
import service.UserService;


@WebServlet(urlPatterns = { "/userManagement" })
public class UserManagementServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	//追加記述(テキストエリアの表示条件)
    	User user = (User) request.getSession().getAttribute("loginUser");
        boolean isShowMessageForm;
        if (user != null) {
            isShowMessageForm = true;
        } else {
            isShowMessageForm = false;
        }

        //追加記述(メッセージ取得し、リクストにメッセージをセット)
        List<User> users = new UserService().getUsers();
        System.out.println(users.size());
        request.setAttribute("users", users);

        request.setAttribute("isShowMessageForm", isShowMessageForm);

    	request.getRequestDispatcher("userManagement.jsp").forward(request, response);
    }
}