package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import service.CommentService;


@WebServlet(urlPatterns = { "/commentText" })
public class CommentServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> messages = new ArrayList<String>();

        if (isValid(request, messages) == true) {

            User user = (User) session.getAttribute("loginUser");
            Comment comment = new Comment();
            comment.setCommentText(request.getParameter("commentText"));
            comment.setMessageId(Integer.parseInt(request.getParameter("messageId")));
            comment.setUserId(user.getId());

            new CommentService().commentRegister(comment);

            response.sendRedirect("./");
        } else {
        	User user = (User) session.getAttribute("loginUser");
    	 	Comment comment = new Comment();
    	 	comment.setCommentText(request.getParameter("commentText"));
         	comment.setMessageId(Integer.parseInt(request.getParameter("messageId")));
         	comment.setUserId(user.getId());
         	session.setAttribute("commentText", comment);
            session.setAttribute("commentMessages", messages);
            response.sendRedirect("./");
        }
    }
    private boolean isValid(HttpServletRequest request, List<String> messages) {

        String commentText = request.getParameter("commentText");



        if (StringUtils.isBlank(commentText) == true) {
            messages.add("コメントを入力してください");
        }

        if (500 < commentText.length()) {
            messages.add("コメントは500文字以下で入力してください");
        }


        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}
