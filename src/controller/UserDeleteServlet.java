package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import service.UserService;

@WebServlet(urlPatterns = {"/userDelete"})
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("userManagement.jsp").forward(request, response);
    }

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException{

		int userId= Integer.parseInt(request.getParameter("userId")) ;
		String userName =request.getParameter("userName");

		UserService userService = new UserService();
		userService.userDelete(userId);//投稿削除

			HttpSession session = request.getSession();
//
			String message = userName+"さんを削除しました";
            session.setAttribute("deleteMessage", message);
//            request.getRequestDispatcher("/userManagement").forward(request, response);
            response.sendRedirect("./userManagement");



	}
}