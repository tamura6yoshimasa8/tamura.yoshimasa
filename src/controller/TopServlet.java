
package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

//追加記述
import beans.User;
import beans.UserComment;
import beans.UserMessage;
import service.CommentService;
import service.MessageService;


@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	//追加記述(テキストエリアの表示条件)
    	User user = (User) request.getSession().getAttribute("loginUser");
        boolean isShowMessageForm;
        if (user != null) {
            isShowMessageForm = true;
        } else {
            isShowMessageForm = false;
        }

        String beforeDate = request.getParameter("beforeDate") ;
        String afterDate = request.getParameter("afterDate");
        String category = request.getParameter("category");
        user.setAfterDate(afterDate);
        user.setBeforeDate(beforeDate);
        user.setCategory(category);


        if(StringUtils.isEmpty(beforeDate)){
        	beforeDate = "2000-01-01";
        }
        System.out.println(beforeDate);

        if(StringUtils.isEmpty(afterDate)){
        	Calendar cal = Calendar.getInstance();
        	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        	afterDate = (sdf.format(cal.getTime()));    //"2050-01-01";
        }


    	afterDate += " 23:59:59";

        if(StringUtils.isBlank(category)){
        	category = null;
        }

        request.setAttribute("retrieval", user);


        //追加記述(メッセージ取得し、リクストにメッセージをセット)
    	List<UserMessage> messages = new MessageService().getMessage( beforeDate, afterDate, category );
        request.setAttribute("messages", messages);


     	//追加記述(コメント取得し、リクストにメッセージをセット)
        List<UserComment> comments = new CommentService().getComment();
        request.setAttribute("comments", comments);

        HttpSession session = request.getSession();

        request.setAttribute("isShowMessageForm", isShowMessageForm);

        List<String> message = new ArrayList<String>();
        if(!StringUtils.isEmpty(user.getBeforeDate()) || !StringUtils.isEmpty(user.getAfterDate()) || !StringUtils.isBlank(user.getCategory())){
        	message.add("～検索～");
        	if(!StringUtils.isEmpty(user.getBeforeDate()) && !StringUtils.isEmpty(user.getAfterDate())){
        		message.add("・"+user.getBeforeDate()+"～"+user.getAfterDate());
        	}else{
        		if(!StringUtils.isEmpty(user.getBeforeDate())){
            		message.add("・"+user.getBeforeDate()+"～");
            	}
            	if(!StringUtils.isEmpty(user.getAfterDate())){
            		message.add("・～"+user.getAfterDate());
            	}
        	}

        	if(!StringUtils.isBlank(user.getCategory())){
        		message.add("・"+user.getCategory());
        	}
        	message.add("検索件数"+ messages.size() +"件");
        }



        request.setAttribute("retrievalMessages", message);


//        Calendar cal = Calendar.getInstance();
//        int year = cal.get(Calendar.YEAR);
//        int month = cal.get(Calendar.MONTH);
//        int date = cal.get(Calendar.DATE);
//
//        String today = (year + "-" + month + "-"+ date);
//        request.setAttribute("today", today);
//        System.out.println(today);




    	request.getRequestDispatcher("/top.jsp").forward(request, response);
    }


}