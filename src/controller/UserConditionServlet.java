
package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import service.UserService;


@WebServlet(urlPatterns = { "/userCondition" })
public class UserConditionServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
	protected void doGet(HttpServletRequest request ,
			HttpServletResponse response) throws IOException ,ServletException{

		response.sendRedirect("userManagement");

	}
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		int isUser = Integer.parseInt(request.getParameter("isUser")) ;
		int userId = Integer.parseInt(request.getParameter("userId")) ;
		String userName = request.getParameter("userName") ;

		UserService userService = new UserService();
		userService.statusChange(isUser ,userId);

		HttpSession session = request.getSession();

		String message;
		if (isUser == 0) {
			message = userName + "さんを復活させました。";
		}else{
			message = userName + "さんを停止させました。";

		}


        session.setAttribute("Messages", message);
        response.sendRedirect("userManagement");
	}
}