package controller;


import java.io.IOException;//4
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;//4
import javax.servlet.annotation.WebServlet;//2
import javax.servlet.http.HttpServlet;//1
import javax.servlet.http.HttpServletRequest;//3
import javax.servlet.http.HttpServletResponse;//3
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;


@WebServlet(urlPatterns = {"/signup"})
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	List<Department> department = new DepartmentService().getDepartment();//後に修正
    List<Branch> branch = new BranchService().getBranch();//後に修正
	@Override
	protected void doGet(HttpServletRequest request ,
			HttpServletResponse response) throws IOException ,ServletException{

		request.setAttribute("department", department);
        request.setAttribute("branch", branch);
		request.getRequestDispatcher("signup.jsp").forward(request, response);

	}
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();

	    request.setAttribute("department", department);
        request.setAttribute("branch", branch);

		if(isValid(request,messages) == true){

			User user = new User();
			user.setLoginId(request.getParameter("loginId"));
			user.setPassword(request.getParameter("password"));
			user.setName(request.getParameter("name"));
			user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
			user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId"))) ;

			new UserService().register(user);

			response.sendRedirect("./userManagement");
		}else{
			User signUpUser = getSignUpUser(request);
			request.setAttribute("signUpUser", signUpUser);
			session.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("signup.jsp").forward(request,response);
		}
	}


	//記入がなかったらエラーをはくメソッド
	private boolean isValid(HttpServletRequest request, List<String> messages) {
        String loginId = request.getParameter("loginId");
        String password = request.getParameter("password");
        String password2 = request.getParameter("password2");
        String name = request.getParameter("name");
        int branchId = Integer.parseInt(request.getParameter("branchId"));
        int departmentId = Integer.parseInt(request.getParameter("departmentId"));


        UserService userService = new UserService();

    	if(userService.getUserLoginId(loginId) != null){
    		messages.add("すでにこのログインIDは使用されています");
    	}

        if (StringUtils.isEmpty(loginId) == true) {
        	messages.add("ログインIDを入力してください");
        }else if((loginId.length() < 6 )|| (loginId.length() > 20) || (!loginId.matches("[a-zA-Z0-9]+"))){
    		messages.add("ログインIDは半角英数字の6文字以上20文字以下で入力してください");
		}

        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください");
        }else if(password.length() < 6 || password.length() > 20 || !password.matches("[ -~。-゜A-Za-z0-9]+")){
        	messages.add("パスワードは半角文字の6文字以上20文字以下で入力してください");
        }

        if(!password.equals(password2)){
        	messages.add("確認用のパスワードと合っていません");
        }

        if (StringUtils.isBlank(name) == true) {
            messages.add("名前を入力してください");
        }else{
        	if(name.length() > 10){
            	messages.add("名前は10文字以内で入力してください");
            }
        }

        if((branchId >1 && departmentId <3) || (branchId ==1 && departmentId ==3) ){
        	messages.add("支店と部署・役職の組み合わせを正しく選択してください");
        }


        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

	 private User getSignUpUser(HttpServletRequest request)
	            throws IOException, ServletException {

	String loginId = request.getParameter("loginId");
	String name = request.getParameter("name");
	int branchId = Integer.parseInt(request.getParameter("branchId"));
	int departmentId = Integer.parseInt(request.getParameter("departmentId"));

	User loginUser = new User();

	loginUser.setLoginId(loginId);
	loginUser.setName(name);
	loginUser.setBranchId(branchId);
	loginUser.setDepartmentId(departmentId);

	return loginUser;
	}

}
