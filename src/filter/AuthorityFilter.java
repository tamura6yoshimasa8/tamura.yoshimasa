package filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;


@WebFilter(filterName="AuthorityFilter",urlPatterns = {"/userManagement","/editing","/signup"})
//@WebFilter(urlPatterns = {"/*"})

public class AuthorityFilter implements Filter {

	@Override
	  public void doFilter(ServletRequest request, ServletResponse response,
	      FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest) request).getSession();
		System.out.println("処理A");
		User user = (User)session.getAttribute("loginUser");

		if((user.getDepartmentId()) != 1){
			System.out.println("処理B");
			String message = "不正なアクセスです。";
			session.setAttribute("Messages", message);
			((HttpServletResponse) response).sendRedirect("./");
			return;
		}


//		if(session.getAttribute("loginUser.departmentId") ){
//			System.out.println("処理B");
//			String errorMessages = "不正なアクセスです。";
//			session.setAttribute("errorMessages", errorMessages);
//			((HttpServletResponse) response).sendRedirect("./top");
//			return;
//		}
		System.out.println("処理C");


	    chain.doFilter(request, response);
	    System.out.println("処理D");

	}

	@Override
  	public void init(FilterConfig config) {



  	}
	@Override
	public void destroy() {
	}

}