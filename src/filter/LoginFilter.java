package filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebFilter(filterName="LoginFilter",urlPatterns="/*")
public class LoginFilter implements Filter {

	@Override
	  public void doFilter(ServletRequest request, ServletResponse response,
	      FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest) request).getSession();
		System.out.println("処理１");
		System.out.println(((HttpServletRequest) request).getServletPath());

		if(!(((HttpServletRequest) request).getServletPath().equals("/login")) &&
				!(((HttpServletRequest) request).getServletPath().matches(".+.css"))&&
				!(((HttpServletRequest) request).getServletPath().matches(".+.js"))){
			System.out.println("処理２");
			if(session.getAttribute("loginUser") == null) {
				System.out.println("処理３");
				String errorMessages = "不正なアクセスです。";
				session.setAttribute("errorMessages", errorMessages);
				((HttpServletResponse) response).sendRedirect("./login");
				return;
			}
		}
		System.out.println("処理４");
		System.out.println("");

	    chain.doFilter(request, response);
	    System.out.println("処理５");
	    System.out.println("");
	    System.out.println("");

	}

	@Override
  	public void init(FilterConfig config) {



  	}
	@Override
	public void destroy() {
	}

}