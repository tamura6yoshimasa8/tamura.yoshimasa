package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exception.SQLRuntimeException;

public class UsersListDao {

    public List<User> getUsersList(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
        	//どんなデータでも受け取ることができるクラス
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("users.*,");
            sql.append("branchs.id,");
            sql.append("branchs.branch_name,");
            sql.append("departments.id,");
            sql.append("departments.department_name ");
            sql.append("FROM (users INNER JOIN branchs ON users.branch_id = branchs.id ) ");
            sql.append("INNER JOIN departments ");
            sql.append("ON users.department_id = departments.id ");

            //SQL文を格納
            ps = connection.prepareStatement(sql.toString());
            //PreparedStatement#executeQueryメソッドでSELECT命令
            //Resultset データベースの結果セットを表すデータのテーブルで、
            //通常、データベースに照会する文を実行することによって生成されます。
            ResultSet rs = ps.executeQuery();
            List<User> ret = toUsersList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUsersList(ResultSet rs)
            throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int usersId = rs.getInt("users.id");
                String usersLoginId= rs.getString("users.login_id");
                String usersPassword = rs.getString("users.password");
                String usersName = rs.getString("users.name");
                String usersBranchName= rs.getString("branchs.branch_name");
                String usersDepartmentName = rs.getString("departments.department_name");
                int usersIsUser= rs.getInt("users.is_user");
                Timestamp usersCreatedDate = rs.getTimestamp("users.created_date");
                Timestamp usersUpdatedDate = rs.getTimestamp("users.updated_date");



                User usersList = new User();
                usersList.setId(usersId);
                usersList.setLoginId(usersLoginId);
                usersList.setPassword(usersPassword);
                usersList.setName(usersName);
                usersList.setBranchName(usersBranchName);
                usersList.setDepartmentName(usersDepartmentName);
                usersList.setIsUser(usersIsUser);
                usersList.setCreatedDate(usersCreatedDate);
                usersList.setUpdatedDate(usersUpdatedDate);



                ret.add(usersList);
            }

            return ret;

        } finally {
            close(rs);
        }

    }

}