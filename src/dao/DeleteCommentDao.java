package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import exception.SQLRuntimeException;

public class DeleteCommentDao {

    public void deleteComment(Connection connection, int commentId) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM ");
            sql.append("comments ");
            sql.append("WHERE ");
            sql.append("id ");
            sql.append("= ");
            sql.append("? ");



            ps = connection.prepareStatement(sql.toString());

            System.out.println(sql.toString());

            ps.setInt(1, commentId);


            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}