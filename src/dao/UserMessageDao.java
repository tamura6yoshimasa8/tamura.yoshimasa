package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.UserMessage;
import exception.SQLRuntimeException;

public class UserMessageDao {

    public List<UserMessage> getUserMessages(Connection connection,
    		String beforeDate, String afterDate, String category, int num) {

        PreparedStatement ps = null;
        try {
        	//どんなデータでも受け取ることができるクラス
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("users.id,");
            sql.append("users.name,");
            sql.append("users.login_id,");
            sql.append("messages.id AS message_id,");
            sql.append("messages.title,");
            sql.append("messages.text,");
            sql.append("messages.category,");
            sql.append("messages.created_date,");
            sql.append("messages.updated_date ");
            sql.append("FROM users ");
            sql.append("INNER JOIN messages ");
            sql.append("ON users.id = messages.user_id ");
            sql.append("WHERE ");
       	 	sql.append("messages.created_date >= ");
            sql.append("? ");
            sql.append("AND ");
            sql.append("messages.created_date <= ");
            sql.append("? ");
            if(!StringUtils.isEmpty(category)){
            	sql.append("AND ");
            	sql.append("messages.category ");
            	sql.append("LIKE ");
                sql.append("?");
            }
            sql.append("ORDER BY created_date DESC limit " + num);
            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, beforeDate);
            ps.setString(2, afterDate);
            if(!StringUtils.isEmpty(category)){
            	ps.setString(3, "%" + category + "%");
            }

            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessageList(ResultSet rs)
            throws SQLException {

        List<UserMessage> ret = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
                int userId = rs.getInt("id");
                String userName = rs.getString("name");
                String userLoginId= rs.getString("login_id");
                int messageId = rs.getInt("message_id");
                String messageTitle = rs.getString("title");
                String messageText = rs.getString("text");
                String messageCategory = rs.getString("category");
                Timestamp messageCreatedDate = rs.getTimestamp("created_date");



                UserMessage message = new UserMessage();
                message.setUserId(userId);
                message.setUserName(userName);
                message.setUserLoginId(userLoginId);
                message.setMessageId(messageId);
                message.setMessageTitle(messageTitle);
                message.setMessageText(messageText);
                message.setMessageCategory(messageCategory);
                message.setMessageCreatedDate(messageCreatedDate);



                ret.add(message);
            }

            return ret;

        } finally {
            close(rs);
        }

    }

}