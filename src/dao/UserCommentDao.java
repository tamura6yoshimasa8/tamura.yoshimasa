package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserComment;
import exception.SQLRuntimeException;

public class UserCommentDao {

    public List<UserComment> getUserComment(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
        	//どんなデータでも受け取ることができるクラス
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("users.id,");
            sql.append("users.name AS user_name,");
            sql.append("users.login_id,");
            sql.append("comments.id AS comment_id,");
            sql.append("comments.text AS comment_text,");
            sql.append("comments.user_id,");
            sql.append("comments.message_id,");
            sql.append("comments.created_date,");
            sql.append("comments.updated_date ");
            sql.append("FROM users ");
            sql.append("INNER JOIN comments ");
            sql.append("ON users.id = comments.user_id ");
//            sql.append("ORDER BY created_date DESC limit ;"+num);

            //SQL文を格納
            ps = connection.prepareStatement(sql.toString());
            //PreparedStatement#executeQueryメソッドでSELECT命令
            //Resultset データベースの結果セットを表すデータのテーブルで、
            //通常、データベースに照会する文を実行することによって生成されます。
            ResultSet rs = ps.executeQuery();
            List<UserComment> ret = toUserCommentList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserComment> toUserCommentList(ResultSet rs)
            throws SQLException {

        List<UserComment> ret = new ArrayList<UserComment>();
        try {
            while (rs.next()) {
                int userId = rs.getInt("id");
                String userName = rs.getString("user_name");
                String userLoginId= rs.getString("login_id");
                int commentId = rs.getInt("comment_id");
                String commentText = rs.getString("comment_text");
                int messageId = rs.getInt("message_id");
                Timestamp commentCreatedDate = rs.getTimestamp("created_date");



                UserComment message = new UserComment();
                message.setUserId(userId);
                message.setUserName(userName);
                message.setUserLoginId(userLoginId);
                message.setCommentId(commentId);
                message.setCommentText(commentText);
                message.setMessageId(messageId);
                message.setCommentCreatedDate(commentCreatedDate);



                ret.add(message);
            }

            return ret;

        } finally {
            close(rs);
        }

    }

}