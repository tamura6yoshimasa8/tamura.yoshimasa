package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Branch;
import exception.SQLRuntimeException;

public class BranchDao {

    public List<Branch> branchSelect(Connection connection) {

        PreparedStatement ps = null;
        try {
        	//どんなデータでも受け取ることができるクラス
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT * ");
            sql.append("FROM branchs ");

//            sql.append("ORDER BY created_date DESC limit ;"+num);

            //SQL文を格納
            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<Branch> ret = toBranchList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<Branch> toBranchList(ResultSet rs)
            throws SQLException {

        List<Branch> ret = new ArrayList<Branch>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String branchName = rs.getString("branch_name");

                Branch message = new Branch();
                message.setBranchId(id);
                message.setBranchName(branchName);

                ret.add(message);
            }

            return ret;

        } finally {
            close(rs);
        }

    }

}