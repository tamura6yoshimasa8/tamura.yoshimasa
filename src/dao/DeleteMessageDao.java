package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import exception.SQLRuntimeException;

public class DeleteMessageDao {

    public void deleteMessage(Connection connection, int messageId) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM ");
            sql.append("messages ");
            sql.append("WHERE ");
            sql.append("id ");
            sql.append("= ");
            sql.append("? ");



            ps = connection.prepareStatement(sql.toString());
            ps.setInt(1, messageId);
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}