package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import exception.SQLRuntimeException;

public class UserConditionDao {

	 public void statusuChangeDao(Connection connection, int isUser, int userId) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET ");
            sql.append("is_user ");
            sql.append("= ");
            sql.append("? ");
            sql.append("WHERE ");
            sql.append("id ");
            sql.append("= ");
            sql.append("? ");



            ps = connection.prepareStatement(sql.toString());

            System.out.println(sql.toString());

            ps.setInt(1, isUser);
            ps.setInt(2, userId);


            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}
