package dao;
import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class UserDao {

	//DBにuser登録
	public void insert(Connection connection, User user){

		//
		PreparedStatement ps = null;

		try{
			//文字の可変シーケンス
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users(");
			sql.append("login_id");
			sql.append(",password");
			sql.append(",name");
			sql.append(",branch_id");
			sql.append(",department_id");
			sql.append(",is_user");
			sql.append(",created_date");
			sql.append(",updated_date");
			sql.append(")VALUES(");
			sql.append("?");//login_id
			sql.append(",?");//password
			sql.append(",?");//name
			sql.append(",?");//branch_id
			sql.append(",?");//department_id
			sql.append(",0");//is_user
			sql.append(", CURRENT_TIMESTAMP");//created_date
			sql.append(", CURRENT_TIMESTAMP");//updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLoginId());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setInt(4, user.getBranchId());
			ps.setInt(5, user.getDepartmentId());
            //実行したいSQL文の値は？にし、引数でSQL文を渡す。
			ps.executeUpdate();//内容を登録する

		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}

	//ユーザー情報取得
	//ユーザーのloginIdとpsswordに該当するユーザーがいるか検証
	public User getUser(Connection connection , String loginId , String password){
		PreparedStatement ps = null;

		try{
			String sql = "SELECT * FROM users WHERE login_id = ? AND password = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, loginId);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
		} catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
	}
	//上ので使う実際にユーザー情報を取得
	private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String password = rs.getString("password");
                String name = rs.getString("name");
                int branchId = rs.getInt("branch_id");
                int departmentId = rs.getInt("department_id");
                int isUser = rs.getInt("is_user");
                Timestamp createdDate = rs.getTimestamp("created_date");
                Timestamp updatedDate = rs.getTimestamp("updated_date");

                User user = new User();
                user.setId(id);
                user.setLoginId(loginId);
                user.setPassword(password);
                user.setName(name);
                user.setBranchId(branchId);
                user.setDepartmentId(departmentId);
                user.setIsUser(isUser);
                user.setCreatedDate(createdDate);
                user.setUpdatedDate(updatedDate);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }


	 //編集画面用ーーーーーーーーーーーーーーーーーーーーーー
    public User getUsers(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE id = ?";

            ps = connection.prepareStatement(sql);
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //追加分(アップデート)ーーーーーーーーーーーーーーーーーーーーーーー
    public void update(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append("  login_id = ?");
            if(!StringUtils.isBlank(user.getPassword())){
            	sql.append(", password = ?");
            }
            sql.append(", name = ?");
            sql.append(", branch_id = ?");
            sql.append(", department_id = ?");
            sql.append(", updated_date = CURRENT_TIMESTAMP");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());

            if(!StringUtils.isBlank(user.getPassword())){
            	ps.setString(1, user.getLoginId());
                ps.setString(2, user.getPassword());
                ps.setString(3, user.getName());
                ps.setInt(4, user.getBranchId());
                ps.setInt(5, user.getDepartmentId());
                ps.setInt(6, user.getId());
            }else{
            	ps.setString(1, user.getLoginId());
                ps.setString(2, user.getName());
                ps.setInt(3, user.getBranchId());
                ps.setInt(4, user.getDepartmentId());
                ps.setInt(5, user.getId());

            }



            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }
    //ログインID重複
    public User getUserLoginId(Connection connection, String loginId) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT login_id, id FROM users WHERE login_id = ? ";


            ps = connection.prepareStatement(sql.toString());
            ps.setString(1, loginId);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserLiginIdList(rs);

        	if (userList.isEmpty() == true) {
                return null;
            } else {
                return userList.get(0);
            }


        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

  //上ので使う実際にユーザー情報を取得
  	private List<User> toUserLiginIdList(ResultSet rs) throws SQLException {

          List<User> ret = new ArrayList<User>();
          try {
              while (rs.next()) {
                  String loginId = rs.getString("login_id");
                  int id = rs.getInt("id");

                  User user = new User();
                  user.setLoginId(loginId);
                  user.setId(id);
                  ret.add(user);
              }
              return ret;
          } finally {
              close(rs);
          }
      }
  	//ユーザー削除
  	public void deleteUser(Connection connection, int userId) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM ");
            sql.append("users ");
            sql.append("WHERE ");
            sql.append("id ");
            sql.append("= ");
            sql.append("? ");



            ps = connection.prepareStatement(sql.toString());
            ps.setInt(1, userId);
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }


}
