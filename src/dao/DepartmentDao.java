package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Department;
import exception.SQLRuntimeException;

public class DepartmentDao {

    public List<Department> DepartmentSelect(Connection connection) {

        PreparedStatement ps = null;
        try {
        	//どんなデータでも受け取ることができるクラス
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT * ");
            sql.append("FROM Departments ");

//            sql.append("ORDER BY created_date DESC limit ;"+num);

            //SQL文を格納
            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<Department> ret = toDepartmentList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<Department> toDepartmentList(ResultSet rs)
            throws SQLException {

        List<Department> ret = new ArrayList<Department>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String DepartmentName = rs.getString("Department_name");

                Department message = new Department();
                message.setDepartmentId(id);
                message.setDepartmentName(DepartmentName);

                ret.add(message);
            }

            return ret;

        } finally {
            close(rs);
        }

    }

}