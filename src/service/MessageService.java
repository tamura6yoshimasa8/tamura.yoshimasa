package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Message;
import beans.UserMessage;
import dao.DeleteMessageDao;
import dao.MessageDao;
import dao.UserMessageDao;

public class MessageService {


	//メッセージ投稿
    public void register(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();

            MessageDao messageDao = new MessageDao();
            messageDao.insert(connection, message);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


    //メッセージ参照(絞り込み機能 追加)

    private static final int LIMIT_NUM = 1000;

    public List<UserMessage> getMessage( String beforeDate , String afterDate, String category) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserMessageDao messageDao = new UserMessageDao();
            List<UserMessage> ret = messageDao.getUserMessages(connection, beforeDate, afterDate, category, LIMIT_NUM);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


    //メッセージ削除

    public void messageDelete(int messageId){

    	Connection connection = null;

    	try{
    		connection = getConnection();

	    	DeleteMessageDao messageDeleteDao = new DeleteMessageDao();//SQL文インスタンス
	    	messageDeleteDao.deleteMessage(connection , messageId);//deleteメソッド発動

    	commit(connection);

	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
    }


}
