package service;


import static utils.CloseableUtil.*;
//固定で使える？
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.User;
import dao.UserConditionDao;
import dao.UserDao;
import dao.UsersListDao;
import utils.CipherUtil;


public class UserService {

	public void register(User user){
		Connection connection = null;
		try{
			//データベースに接続
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            UserDao userDao = new UserDao();
            userDao.insert(connection, user);

            commit(connection);
		} catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}

	 //全ユーザー参照

    private static final int LIMIT_NUM = 1000;

    public List<User> getUsers() {

        Connection connection = null;
        try {
            connection = getConnection();

            UsersListDao userDao = new UsersListDao();
            List<User> ret = userDao.getUsersList(connection, LIMIT_NUM);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	 //ユーザー参照


    public User getUser(int userId) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            User user =  userDao.getUsers(connection, userId);


            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


    //ユーザー復活・停止変更
    public void statusChange(int isUser ,int userId) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserConditionDao userConditionDao = new UserConditionDao();
            userConditionDao.statusuChangeDao(connection, isUser, userId);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //追加記述(アップデート)
    public void update(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            if(!StringUtils.isBlank(user.getPassword())){
            	String encPassword = CipherUtil.encrypt(user.getPassword());
                user.setPassword(encPassword);
            }

            UserDao userDao = new UserDao();
            userDao.update(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //ログインID重複
    public User getUserLoginId(String loginId) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            User ret = userDao.getUserLoginId(connection, loginId );

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


    //ユーザー削除

    public void userDelete(int userId){

    	Connection connection = null;

    	try{
    		connection = getConnection();

	    	UserDao userDao = new UserDao();//SQL文インスタンス
	    	userDao.deleteUser(connection , userId);//deleteメソッド発動

    	commit(connection);

	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }




    }

}
