package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Comment;
import beans.UserComment;
import dao.CommentDao;
import dao.DeleteCommentDao;
import dao.UserCommentDao;

public class CommentService {


	//メッセージ投稿
    public void commentRegister(Comment message) {

        Connection connection = null;
        try {
            connection = getConnection();

            CommentDao commentDao = new CommentDao();
            commentDao.commentInsert(connection, message);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


    //メッセージ参照

    private static final int LIMIT_NUM = 1000;

    public List<UserComment> getComment() {

        Connection connection = null;
        try {
            connection = getConnection();

            UserCommentDao commentDao = new UserCommentDao();
            List<UserComment> ret = commentDao.getUserComment(connection, LIMIT_NUM);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void commentDelete(int commentId){

    	Connection connection = null;

    	try{
    		connection = getConnection();

	    	DeleteCommentDao commentDeleteDao = new DeleteCommentDao();//SQL文インスタンス
	    	commentDeleteDao.deleteComment(connection , commentId);//deleteメソッド発動

    	commit(connection);

	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }




    }
}
