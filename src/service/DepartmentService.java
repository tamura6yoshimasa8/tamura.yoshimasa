package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Department;
import dao.DepartmentDao;

public class DepartmentService {


	//Department情報取得
    public List<Department> getDepartment() {

        Connection connection = null;
        try {
            connection = getConnection();

            DepartmentDao DepartmentDao = new DepartmentDao();
            List<Department> ret = DepartmentDao.DepartmentSelect(connection);

            commit(connection);

            return ret;

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}
