package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Branch;
import dao.BranchDao;

public class BranchService {


	//branch情報取得
    public List<Branch> getBranch() {

        Connection connection = null;
        try {
            connection = getConnection();

            BranchDao branchDao = new BranchDao();
            List<Branch> ret = branchDao.branchSelect(connection);

            commit(connection);

            return ret;

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}
